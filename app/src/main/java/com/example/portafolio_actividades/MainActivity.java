package com.example.portafolio_actividades;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;

public class MainActivity extends AppCompatActivity implements View.OnClickListener {
    Button btnLogin, btnInfo;
    EditText etUsuario, etPassword;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        btnLogin = findViewById(R.id.btnLogin);
        btnLogin.setOnClickListener(this);
        btnInfo=findViewById(R.id.btnInfo);
        btnInfo.setOnClickListener(this);
    }

    @Override
    public void onClick(View view) {
        //enlazamos las variables con el layout
        switch (view.getId()){
            case R.id.btnLogin:
                etUsuario = findViewById(R.id.txtUsuario);
                etPassword = findViewById(R.id.txtContraseña);
                //declarar 2 variables que me van a recuperar la informacion que ingresemos en la caja de texto
                String elusuario = etUsuario.getText().toString();
                String elPassword = etPassword.getText().toString();

                //validar si el usuario esta vacio o no
                if (!elusuario.isEmpty() && !elPassword.isEmpty()) {

                    Log.i("Etiqueta", "prueba onclick login");
                    Intent intentLogin = new Intent(this, Lista.class);
                    //pasar valores
                    intentLogin.putExtra("valorUsuario", elusuario);
                    intentLogin.putExtra("valorPassword", elPassword);


                    startActivity(intentLogin);
                } else {
                    etUsuario.setError("El usuario es obligatorio");
                    etPassword.setError("La contraseña es obligatoria");
                }
                break;
            case R.id.btnInfo:

                Intent i = new Intent(this, InfoActivity.class);
                startActivity(i);
                break;

        }

    }
}

package com.example.portafolio_actividades;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

public class Traductor extends AppCompatActivity implements View.OnClickListener {
    TextView trad;
    EditText texto;
    Button acc;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_traductor);
        trad = findViewById(R.id.traduccion);
        texto = findViewById(R.id.textito);
        acc = findViewById(R.id.Accion);

        acc.setOnClickListener(this);
    }

    @Override
    public void onClick(View view) {
        String valor = texto.getText().toString();

        switch(valor)
        {
            case "Actividad":
                trad.setText("Activity");
                break;
            case "Fragmento":
                trad.setText("Fragment");
                break;
            case "Menu":
                trad.setText("Main");
                break;
            case "Publico":
                trad.setText("Public");
                break;
            case "Estudio":
                trad.setText("Studio");
                break;
            case "Arreglo":
                trad.setText("Array");
                break;
            case "Protegido":
                trad.setText("Protected");
                break;
            case "Hola":
                trad.setText("Hello");
                break;
            case "Vista":
                trad.setText("View");
                break;
            case "Evento":
                trad.setText("La traduccion es "+"Event");
                break;
            case "boton":

                trad.setText("La traduccion es "+"Butom");
                break;
            case "perfil":
                trad.setText("Profile");
                break;
            case "importar":
                trad.setText("Import");
                break;
            case "sistema":
                trad.setText("System");
                break;
            case "imprimir":
                trad.setText("Print");
                break;
            case "satisfactoriamente":
                trad.setText("Successfully");
                break;
            case "tarea":
                trad.setText("Task");
                break;
            case "emulador":
                trad.setText("La traduccion es "+"Emulator");
                break;
            case "archivo":
                trad.setText("File");
                break;
            case "requerir":
                trad.setText("Require");
                break;
            default:
                Toast.makeText(getApplicationContext(), "Palabra no encontrada", Toast.LENGTH_SHORT).show();
                break;
        }

    }
}
